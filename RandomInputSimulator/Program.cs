﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsInput;
using WindowsInput.Native;

namespace RandomInputSimulator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Running...");
            Console.WriteLine("Press esc to quit after random wait.");

            var random = new Random((int)DateTime.Now.Ticks);
            var sleepTime = random.Next(10000, 60000);

            var nextKey = random.Next(0, 4);
            var keys = new List<VirtualKeyCode> { VirtualKeyCode.SPACE, VirtualKeyCode.VK_W, VirtualKeyCode.VK_A, VirtualKeyCode.VK_S, VirtualKeyCode.VK_D };

            var nextKeyTime = random.Next(500, 1000);
            var simulator = new InputSimulator();

            do
            {
                while (!Console.KeyAvailable)
                {
                    simulator.Keyboard.KeyDown(keys[nextKey]);
                    Thread.Sleep(nextKeyTime);
                    simulator.Keyboard.KeyUp(keys[nextKey]);

                    Console.WriteLine("Random wait time {0} ms, press key {1} for {2} ms", sleepTime, keys[nextKey], nextKeyTime);

                    Thread.Sleep(sleepTime);
                    sleepTime = random.Next(1000, 60000);
                    nextKey = random.Next(0, 4);
                    nextKeyTime = random.Next(500, 1000);
                }
            }
            while (Console.ReadKey(true).Key != ConsoleKey.Escape);
            
        }
    }
}
